<?php

use HuonCS\FormBuilder\FormBuilder;

require '../../vendor/autoload.php';

$result = ['ok' => true];

try {
    $form = @$_POST['form'];
    $op = @$_POST['op'];
    $handler = @$_POST['handler'];

    if (!preg_match('/^[-_a-zA-Z0-9]+$/',$form)) throw new Exception('Form ID missing');
    if (!preg_match('/^(run|make|build)?$/',$op)) throw new Exception('Operation must be one of "run", "build" or "make", defaulting to "make".');
    if (!preg_match('/./',$handler)) throw new Exception('Handler not specified.');

    $fb = new FormBuilder($form, $_SERVER['DOCUMENT_ROOT']);
    if ($op == 'build') {
    	$fb->build();
    } else if ($op == 'make') {
    	$fb->make();
    }
    if (!$fb->compiled()) throw new Exception('Unable to find or compile the requested form.');
    $html = $fb->generate($form,$handler);
    $result['html'] = $html;
}
catch (Exception $e) {
    $result['ok'] = false;
    $result['error'] = $e->getMessage();
}
finally {
    $json = json_encode($result);
    if (!$json) $json = json_encode(['ok' => false, 'error' => 'JSON encoding error #'.json_last_error()]);

    header('Content-type: application/json');
    print $json;
}
