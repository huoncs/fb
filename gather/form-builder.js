(function ($) {

	$.fn.formbuilder = function (options, ready) {
		return this.each(function () {
			var $control = $(this);
			$.ajax({	
				data: options,
		       	dataType: 'json',
		       	method: 'POST',
		       	url: '/g/ajax.php',
		       	success: function (data,status,xhr) {
		       		if (data.ok == 0) {
		       			$control.html('Error: '+data.error);
		       		} else {
		       			$control.html(data.html);
		       			if (ready) ready($control);
		       		}
		       	},
		       	error: function (xhr,status,error) {
		       		$control.html('Error: '+error);
		       	}
			});
		});
	};

}(jQuery));

