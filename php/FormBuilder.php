<?php # form-builder.php

namespace HuonCS\FormBuilder;

class FormBuilder extends FormBase
{
    private $values;
    private $message;
    private $error;

    public function __construct($filename, $root)
    {
        parent::__construct($filename, $root);
        $this->root = $root;
        $this->values = [];
        $this->message = null;
        $this->error = null;
    }

    public function make()
    {
        $intime = filemtime($this->infile);
        $outtime = filemtime($this->outfile);

        if ($outtime === FALSE || $outtime < $intime) {
            $this->build();
        }
    }

    public function build()
    {
        $fc = new FormCompiler($this->filename, $this->root);
        $fc->compile();
        $fc->save();
    }

    public function compiled()
    {
        return file_exists($this->outfile);
    }

    public function setValues($values)
    {
        $this->values = $values;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setErrorMessage($error)
    {
        $this->error = $error;
    }

    public function generate($name, $handler)
    {
        $json = file_get_contents($this->outfile);
        if (!$json) throw new Exception('Empty file: '.$this->outfile);

        $definition = @json_decode($json,true);
        if (!$definition) throw new Exception('JSON error: '.json_last_error());

        $html = '';

        $uploader = @$definition['is_uploader'] ? 'enctype="multipart/form-data" ' : '';
        
        $html .= sprintf('<form class="f-b" name="%s" %smethod="post" action="%s">', $name, $uploader, $handler);
        $html .= sprintf('<input type="hidden" name="form" value="%s">',$this->filename);
        if (@$definition['is_uploader']) $html .= '<input type="hidden" name="MAX_FILE_SIZE" value="'.FormBase::MAX_FILE_SIZE.'" />';
        if ($this->error) $html .= sprintf('<div class="error">%s</div>',$this->error);
        if ($this->message) $html .= sprintf('<div class="ok">%s</div>',$this->message);

        $form = $definition['form'];
        $grid = [];

        foreach ($form as $r => $each) {
            switch ($each[0]) {
                case 'HR':
                    $html .= '<hr>';
                    $grid[] = null;
                    break;
                case 'LEGEND':
                    $html .= sprintf('<h3>%s</h3>',htmlentities($each[1]));
                    $grid[] = null;
                    break;
                case 'TEXT':
                    $text = $each[1];
                    if (preg_match('/^#(\S+)\s*(.*)$/',$text,$matches)) {
                        $attr = " id=\"{$matches[1]}\"";
                        $text = $matches[2];
                    } else {
                        $attr = "";
                    }

                    $html .= sprintf('<p%s>%s</p>',$attr,htmlentities($text));
                    $grid[] = null;
                    break;
                case 'ROW':
                    $row = $each[1];
                    $html .= '<div class="row">';
                    foreach ($row as $c => $spec) {
                        if ($spec['null']) continue;

                        $id = $spec['id'];
                        $rows = $spec['rows'];
                        $columns = $spec['columns'];

                        $field = $definition['fields'][$id];
                        $html .= sprintf('<label class="%s rows-%s columns-%s">', $field['type']['base'], $rows, $columns);

                        $label = $field['label'];
                        if (preg_match('#^(.+?)\s*/\s*(.+)$#',$label,$matches)) {
                            if ($matches[1] == '---') {
                                $label = '&nbsp;';
                            } else {
                                $label = htmlentities($matches[1]) . ':';
                            }
                            $note = ' <i>'.htmlentities($matches[2]).'</i>';
                        } elseif ($label) {
                            if ($label == '---') {
                                $label = '&nbsp;';
                            } else {
                                $label = htmlentities($label) . ':';
                            }
                            $note = '';
                        } else {
                            $label = '';
                            $note = '';
                        }
                        if ($label || $note) $html .= sprintf('<span>%s%s</span>',$label,$note);

                        switch ($field['type']['base']) {
                            case 'string':
                                $html .= $this->generateInput('text',$id,$field);
                                break;
                            case 'select':
                                $html .= $this->generateSelect($id,$field,$definition['arrays'][$field['type']['id']]);
                                break;
                            case 'generate':
                                $html .= $this->generateGenerate($id,$field);
                                break;
                            case 'number':
                                $html .= $this->generateInput('number',$id,$field);
                                break;
                            case 'email':
                                $html .= $this->generateInput('email',$id,$field);
                                break;
                            case 'password':
                                $html .= $this->generateInput('password',$id,$field,false);
                                break;
                            case 'date':
                                $html .= $this->generateInput('date',$id,$field,false);
                                break;
                            case 'file':
                                $html .= $this->generateInput('file',$id,$field,false);
                                break;
                            case 'text':
                                $html .= $this->generateTextArea($id,$field);
                                break;
                            case 'block':
                                $html .= $this->generateBlock($id,$field);
                                break;
                            case 'button':
                                $html .= $this->generateButton($id,$field);
                                break;
                            default:
                                throw new Exception('Unknown type: '.$field['type']['base']);
                        }
                        $html .= '</label>';
                    }
                    $html .= '</div>';
                    break;
            }
        }
        $buttons = $definition['buttons'] ?: ['label' => 'Send', 'type' => 'submit'];
        $html .= '<div class="buttons">';
        foreach ($buttons as $button) $html .= sprintf('<input type="%s" class="btn_%s" name="%s" value="%s">', $button['type'], strtolower($button['label']), strtolower($button['label']), $button['label']);
        $html .= '</div>';
        $html .= '</form>';

        return $html;
    }

    private function generateInput($type, $id, $field, $use_value = true)
    {
        return sprintf('<input type="%s" class="field inp_%s" name="%s" value="%s"%s%s>',
               $type, $id, $id, $use_value ? @$this->values[$id] ?: '' : '',
               $field['required'] ? ' required' : '',
               $field['placeholder'] ? sprintf(' placeholder="%s"',htmlentities($field['placeholder'])) : '');
    }

    private function generateSelect($id, $field, $options)
    {
        $html = '';
        $html .= sprintf('<select class="field sel_%s" name="%s">',$id,$id);
        foreach ($options as $option) {
            $selected = @$this->values[$id] == $option['id'] ? ' selected' : '';
            $html .= sprintf('<option value="%s"%s>%s</option>', $option['id'], $selected,htmlentities($option['text']));
        }
        $html .= '</select>';
        return $html;
    }


    private function generateGenerate($id, $field)
    {
        $html = '';
        $html .= sprintf('<select class="field gen_%s" name="%s" data-source="%s">',$id,$id,$field['type']['id']);
        $html .= '</select>';
        return $html;
    }

    private function generateTextArea($id, $field)
    {
        return sprintf('<textarea class="field txt_%s" name="%s"%s%s%s>%s</textarea>',
               $id, $id,
               @$field['type']['lines'] ? sprintf(' class="lines-%s"',$field['type']['lines']) : '',
               $field['required'] ? ' required' : '',
               $field['placeholder'] ? sprintf(' placeholder="%s"',htmlentities($field['placeholder'])) : '',
               @$this->values[$id] ?: '');
    }

    private function generateBlock($id, $field)
    {
        return sprintf('<div class="field block blk_%s"></div>',
               $id);
    }

    private function generateButton($id, $field)
    {
        return sprintf('<button class="field button btn_%s">%s</button>',
               $id, htmlentities($field['placeholder']));
    }
}
