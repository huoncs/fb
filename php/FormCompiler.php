<?php # form-compiler.php

namespace HuonCS\FormBuilder;

class FormCompiler extends FormBase
{
    private $source;
    private $form;
    private $fields;
    private $buttons;
    private $aliases;
    private $arrays;

    public function __construct($filename, $root)
    {
        parent::__construct($filename,$root);
        $this->source = @file($this->infile);
        if (!$this->source) throw $this->error('file not found',-1);
    }

    private function addFormRow($row,$i)
    {
        $actual = [];
        $full = 12;
        $share = 0;
        foreach ($row as $i => $element) {
            $spec = $this->interpretFieldSpec($element);
            if ($spec['null']) {
                $f = count($this->form);
                if ($f && $this->form[$f-1][0] == 'ROW') { // it might be a continuation of a multi-row field
                    $isrowspan = false;
                    while (true) {
                        $prev = $this->form[--$f][1][$i];
                        if (!$prev['rows']['null']) break;
                        $isrowspan = true;
                    }
                    if ($isrowspan) {
                        $prevcols = $prev['columns'];
                        $spec['columns'] = $prevcols;
                        $full -= $prevcols;
                    }
                } // else it's probably just an HR, so ignore
            } else {
                if (array_key_exists($spec['id'], $this->fields)) throw $this->error(sprintf('Reusing field "%s"',$spec['id']),$i);
                $this->fields[$spec['id']] = [];
                if ($spec['columns']) {
                    $full -= $spec['columns'];
                } else {
                    $share++;
                }
            }
            $actual[] = $spec;
        }

        if ($share) {
            $rest = floor($full / $share);
            $first = $full - ($share-1) * $rest; // ensuring it adds up to twelve!
            foreach ($actual as $i => $spec) {
                if (!$spec['columns']) {
                    $actual[$i]['columns'] = $first;
                    $first = $rest;
                }
            }
        }

        if (count($actual) == 1 && $actual[0]['null']) {
            $this->form[] = ['HR'];
        } else {
            $this->form[] = ['ROW',$actual];
        }
    }

    private function addFormLegend($legend)
    {
        $this->form[] = ['LEGEND',$legend];
    }

    private function addFormText($text)
    {
        $this->form[] = ['TEXT',$text];
    }

    private function interpretID($id)
    {
        if (preg_match('/^-/',$id)) {
            return null;
        } else {
            return $id;
        }
    }

    private function interpretFieldSpec($spec)
    {
        if (preg_match('/^-/',$spec)) {
            return ['null' => true, 'columns' => null, 'rows' => 1];
        } elseif (preg_match('#^([a-zA-Z0-9_]+)[(](\d*)/(\d+)[)]$#',$spec,$matches)) {
            return ['null' => false, 'id' => $matches[1], 'columns' => @+$matches[2] ?: null, 'rows' => +$matches[3]];
        } elseif (preg_match('/^([a-zA-Z0-9_]+)[(](\d+)[)]$/',$spec,$matches)) {
            return ['null' => false, 'id' => $matches[1], 'columns' => +$matches[2], 'rows' => 1];
        } else {
            return ['null' => false, 'id' => $spec, 'columns' => null, 'rows' => 1];
        }
    }

    private function addFormButton($button,$i)
    {
        switch ($button[0]) {
            case '[':
                $type = 'submit';
                break;
            case '<':
                $type = 'button';
                break;
            default:
                die('No button type');
        }
        $this->buttons[] = ['label' => substr($button,1), 'type' => $type];
    }

    private function clearObject()
    {
        $this->form = [];
        $this->fields = [];
        $this->buttons = [];
        $this->aliases = [];
        $this->arrays = [];
        $this->isUploader = false;
    }

    private function addTypeAlias($alias,$type,$i)
    {
        $this->aliases[$alias] = $type;
    }

    private function defineField($field,$type,$required,$placeholder,$label,$i)
    {
        if (!array_key_exists($field, $this->fields)) throw $this->error(sprintf('Defining unused field "%s"',$field),$i);
        if (count($this->fields[$field]) > 0) throw $this->error(sprintf('Redefining field "%s"',$field,$i));
        $this->fields[$field] = ['type' => $this->interpretType($type,$i),
                                 'required' => ($required == '*'),
                                 'placeholder' => $this->interpretLabel($placeholder),
                                 'label' => $this->interpretLabel($label)];
    }

    private function interpretType($type,$i)
    {
        if (($alias = @$this->aliases[$type])) $type = $alias;
        if ($type == 'file') $this->isUploader = true;
        
        if (preg_match('/^(email|password|text|date|file)$/',$type,$matches)) return ['base' => $matches[1]];
        if (preg_match('/^(text)[(](\d+)[)]$/',$type,$matches)) return ['base' => $matches[1], 'lines' => +$matches[2]];
        if (preg_match('/^(string)[(](\d+)[)]$/',$type,$matches)) return ['base' => $matches[1], 'length' => +$matches[2]];
        if (preg_match('/^(number)[(](\d+)\s*,\s*(\d+)[)]$/',$type,$matches)) return ['base' => $matches[1], 'length' => +$matches[2], 'places' => +$matches[3]];
        if (preg_match('/^(select)[(]([a-zA-Z0-9_]+)[)]$/',$type,$matches)) return ['base' => $matches[1], 'id' => $matches[2]];
        if (preg_match('/^(generate)[(]([a-zA-Z0-9_]+)[)]$/',$type,$matches)) return ['base' => $matches[1], 'id' => $matches[2]];
        if (preg_match('/^(button)$/',$type,$matches)) return ['base' => $matches[1]];
        if (preg_match('/^(block)$/',$type,$matches)) return ['base' => $matches[1]];
        throw $this->expectation('raw type',$type,$i);
    }

    private function interpretLabel($label)
    {
        return preg_replace(['/^"/','/"$/','/\\\\"/'],['','','"'],$label);
    }

    private function addArrayItem($array,$id,$text,$i)
    {
        $this->arrays[$array][] = ['id' => $this->interpretID($id), 'text' => $this->interpretLabel($text)];
    }

    public function compile()
    {
        // regexps for use in parser
        $ID = '(?:[a-z][a-zA-Z0-9]*(?:_[a-z][a-zA-Z0-9]*)*)';
        $NULL = '(?:---+)';
        $TYPE = "(?:$ID(?:[(](?:\d+|$ID)(?:\s*,\s*(?:\d+|$ID))*[)])?)";
        $LABEL = '(?:["](?:[\\\\].|[^\\\\"])*["])';
        $ROWCOL = '(?:[(](?:\d*/\d+|\d+)[)])?';

        $this->clearObject();

        $state = 'STARTING';

        foreach ($this->source as $i => $line) {
            // remove leading and trailing spaces and comments, and skip blank lines completely
            $line = preg_replace(['/^#.*$/','/\s+#.*$/'], ['',''], trim($line));
            if ($line == '') continue;

            // convert \# back to # inside strings (or wherever)
            $line = preg_replace('/\\\\#/','#', $line);

            if (preg_match("/^form\s*:$/",$line)) {
                $state = 'FORM';
            } elseif (preg_match('/^type\s*:$/',$line)) {
                $state = 'TYPE';
            } elseif (preg_match('/^field\s*:$/',$line)) {
                $state = 'FIELD';
            } elseif (preg_match("/^array\s+($ID)\s*:$/",$line,$matches)) {
                $state = 'ARRAY';
                $array = $matches[1];
            } elseif ($state == 'FORM') {
                if (preg_match('/^\[\s*(.+?)\s*\]$/',$line,$matches)) {
                    $legend = $matches[1];
                    $this->addFormLegend($legend);
                } elseif (preg_match("/^$NULL\s*;$/",$line)) {
                    $this->addFormRow(['--'],$i);
                } elseif (preg_match("#^((?:$NULL|$ID$ROWCOL)(?:\s+(?:$NULL|$ID$ROWCOL))*)\s*;$#",$line,$matches)) {
                    $row = preg_split('/\s+/',$matches[1]);
                    $this->addFormRow($row,$i);
                } elseif (preg_match("/^($LABEL)\s*;$/",$line,$matches)) {
                    $label = $matches[1];
                    $this->addFormText($this->interpretLabel($label));
                } elseif (preg_match("/^(?:[<][^]>]+[>]\s*|[[][^]>]+[]]\s*)+;$/",$line) && preg_match_all("/([[<][^]>]+)[]>]/",$line,$matches)) {
                    foreach ($matches[1] as $button) {
                        $this->addFormButton($button,$i);
                    }
                } else {
                    throw $this->expectation('form layout', $line, $i);
                }
            } elseif ($state == 'TYPE') {
                if (preg_match("/^($ID)\s*=\s*($TYPE)\s*;$/",$line,$matches)) {
                    $this->addTypeAlias($matches[1],$matches[2],$i);
                } else {
                    throw $this->expectation('type definition', $line, $i);
                }
            } elseif ($state == 'FIELD') {
                if (preg_match("/^($TYPE)\s+([*]?)\s*($ID)\s*($LABEL)?\s*(?::\s*($LABEL)\s*)?;$/",$line,$matches)) {
                    @list ($_, $type, $required, $field, $placeholder, $label) = $matches;
                    $this->defineField($field,$type,$required,$placeholder,$label,$i);
                } else {
                    throw $this->expectation('field definition', $line, $i);
                }
            } elseif ($state == 'ARRAY') {
                if (preg_match("/^($ID|$NULL)\s*=\s*($LABEL)\s*;$/",$line,$matches)) {
                    list($_, $id, $text) = $matches;
                    $this->addArrayItem($array,$id,$text,$i);
                } else {
                    throw $this->expectation('array item', $line, $i);
                }
            } else {
                throw $this->expectation('form, type, field or array',$line,$i);
            }
        }
    }

    public function save()
    {
        file_put_contents($this->outfile,
                          json_encode(['form' => $this->form,
                                       'is_uploader' => $this->isUploader,
                                       'fields' => $this->fields,
                                       'buttons' => $this->buttons,
                                       'arrays' => $this->arrays]));
    }

    public function expectation($expected, $got, $i)
    {
        return new Exception(sprintf('Line %d of %s: expected %s, got "%s".', $i+1, $this->filename, $expected, $got));
    }

    public function error($message, $i)
    {
        return new Exception(sprintf('Line %d of %s: %s.', $i+1, $this->filename, $message));
    }
}
