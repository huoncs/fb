<?php declare(strict_types = 1);

namespace HuonCS\FormBuilder;

use Illuminate\Database\Capsule\Manager;

class Database
{
    protected $db;
    protected $project;

    public function __construct($project)
    {
        $this->project = $project;
        $capsule = new Manager();

        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'formbuilder',
            'username'  => 'formbuilder',
            'password'  => 'ZVXxi1DGeq1WEKC4',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], 'formbuilder');

        $capsule->setAsGlobal();

        $this->db = Manager::connection('formbuilder');
    }

    public function getFormNames()
    {
        $collection = $this->db->table('form')
            ->select('name')
            ->where('project','=',$this->project)
            ->get();
        return array_map(function ($i) { return $i->name; }, $collection->toArray());
    }

    public function saveForm($name, $source, $compiled = null)
    {
        $this->db->table('form')
            ->updateOrInsert(
                ['project' => $this->project, 'name' => $name],
                ['source' => $source, 'compiled' => $compiled]);
    }

    public function formExists($name)
    {
        $collection = $this->db->table('form')
            ->select('name')
            ->where('project','=',$this->project)
            ->where('name','=',$name)
            ->get();

        return $collection->isNotEmpty();
    }

    public function getSource($name)
    {
        $collection = $this->db->table('form')
            ->select('source')
            ->where('project','=',$this->project)
            ->where('name','=',$name)
            ->get();

        if ($collection->isEmpty()) {
            return null;
        } else {
            return $collection->get(0)->source;
        }
    }
}
