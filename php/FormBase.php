<?php # form-base.php

namespace HuonCS\FormBuilder;

class FormBase
{
    public const MAX_FILE_SIZE = 256*1024;

    protected $home;
    protected $filename;
    protected $infile;
    protected $outfile;

    public function __construct($filename, $root)
    {
        $this->filename = $filename;

        $this->home = $root;
        $this->infile = sprintf('%s/forms/%s.form',$this->home,$filename);
        $this->outfile = sprintf('%s/forms/compiled/%s.fo',$this->home,$filename);
    }
}
