<?php

namespace HuonCS\FormBuilder;


class Utilities
{
    public static function returnAJAX($args, $error = null)
    {
        if ($error) {
            $args['ok'] = false;
            $args['error'] = $error;
        } else {
            $args['ok'] = true;
        }

        header('Content-type: application/json');
        print json_encode($args);
        exit(0);
    }

    public static function returnFormContents($dir, $filename)
    {
        if (preg_match('/^[-_a-z0-9]+$/',$form) && file_exists($dir."forms/{$form}.form")) {
            static::returnAJAX(['source' => file_get_contents($dir."forms/{$form}.form")]);
        } else {
            static::returnAJAX(['form' => $form], 'Form not found.');
        }
    }

    public static function returnFormFileList($dir)
    {
        $names = glob($dir.'forms/*.form');
        $list = array_map(function ($x) { return basename($x,'.form'); }, $names)];        
        static::returnAJAX(['forms' => $list]);        
    }
}